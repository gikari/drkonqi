/*******************************************************************
* reportassistantpages_bugzilla.h
* Copyright 2009, 2011    Dario Andres Rodriguez <andresbajotierra@gmail.com>
* Copyright 2019 Harald Sitter <sitter@kde.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************/

#ifndef REPORTASSISTANTPAGES__BUGZILLA__H
#define REPORTASSISTANTPAGES__BUGZILLA__H

#include "reportassistantpage.h"

#include "reportassistantpages_base.h"

#include "ui_assistantpage_bugzilla_login.h"
#include "ui_assistantpage_bugzilla_information.h"
#include "ui_assistantpage_bugzilla_preview.h"
#include "ui_assistantpage_bugzilla_send.h"

#include <clients/bugfieldclient.h>

namespace KWallet { class Wallet; }
class KCapacityBar;

/** Bugzilla login **/
class BugzillaLoginPage: public ReportAssistantPage
{
    Q_OBJECT

public:
    explicit BugzillaLoginPage(ReportAssistantDialog *);
    ~BugzillaLoginPage() override;

    void aboutToShow() override;
    bool isComplete() override;

private Q_SLOTS:
    void loginClicked();
    bool canLogin() const;
    void login();
    void loginFinished(bool);
    void loginError(const QString &error);

    void walletLogin();

    void updateLoginButtonStatus();

Q_SIGNALS:
    void loggedTurnToNextPage();

private:
    void updateWidget(bool enabled);
    bool kWalletEntryExists(const QString&);
    void openWallet();

    Ui::AssistantPageBugzillaLogin      ui;

    KWallet::Wallet *                   m_wallet;
    bool                                m_walletWasOpenedBefore;
    bool                                m_bugzillaVersionFound;
};

/** Title and details page **/
class BugzillaInformationPage : public ReportAssistantPage
{
    Q_OBJECT

public:
    explicit BugzillaInformationPage(ReportAssistantDialog *);

    void aboutToShow() override;
    void aboutToHide() override;

    bool isComplete() override;
    bool showNextPage() override;

private Q_SLOTS:
    void showTitleExamples();
    void showDescriptionHelpExamples();

    void checkTexts();
    void loadDistroCombo();

private:
    void setDistros(const Bugzilla::BugField::Ptr &field);
    void setDistroComboError(const QString &error);
    int currentDescriptionCharactersCount();

    Ui::AssistantPageBugzillaInformation    ui;
    KCapacityBar *                          m_textCompleteBar;

    bool                                    m_textsOK;

    int                                     m_requiredCharacters;
};

/** Preview report page **/
class BugzillaPreviewPage : public ReportAssistantPage
{
    Q_OBJECT

public:
    explicit BugzillaPreviewPage(ReportAssistantDialog *);

    void aboutToShow() override;
    void aboutToHide() override;

private:
    Ui::AssistantPageBugzillaPreview    ui;
};

/** Send crash report page **/
class BugzillaSendPage : public ReportAssistantPage
{
    Q_OBJECT

public:
    explicit BugzillaSendPage(ReportAssistantDialog *);

    void aboutToShow() override;

private Q_SLOTS:
    void sent(int);
    void sendError(const QString &errorString);

    void retryClicked();
    void finishClicked();

    void openReportContents();

private:
    Ui::AssistantPageBugzillaSend           ui;
    QString                                 reportUrl;

    QPointer<QDialog>                       m_contentsDialog;

Q_SIGNALS:
    void finished(bool);
};

#endif
